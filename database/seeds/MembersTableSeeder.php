<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = new \App\Member([
        'id' => 1,
        'mname' => 'Arvind Narayanan',
        'post' => 'Junior web editor',
        'email' => 'arvindonarayanan@gmail.com',
        'facebook' => 'facebook.com/arvindonarayanan',
        'username' => 'Arvind',
        'password' => bcrypt('H!ghW@y2H3ll'),
        'isadmin' => true,
        'category' => 4
      ]);

      $user->save();
    }
}
