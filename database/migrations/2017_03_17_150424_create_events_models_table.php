<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_models', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->string('ename')->default('Event name');
            $table->date('date');
            $table->string('description')->default('Event description');
              $table->string('image')->default('EventPictures/default.jpg');
             $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_models');
    }
}
