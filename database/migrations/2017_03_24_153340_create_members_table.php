<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mname')->default('Member name');
            $table->string('post')->default('Member post');
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('email')->default('Member email');
            $table->string('username')->default('Member username');
            $table->string('achievements')->default('achievements')->nullable();
            $table->string('password')->default('Member password');
            $table->boolean('isadmin',3)->default(false);
            $table->integer('category')->default(1);
            $table->string('image')->default('css/resources/images/default.jpg');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
