@extends('template.template')

@section('head')
  @include('sponsors.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')

  <div class="csi-sponsors-main">
    <div class="csi-sponsor-title">
      <h1>OUR SPONSORS</h1>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/lubricants.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>HP Lubricants</h3>
        <p>Power Sponsor</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/kuruvila_finance.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Kuruvila Finance</h3>
        <p>Finance Partner</p>
      </div>
    </div>

    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/liberty.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Liberty Proless Works</h3>
        <p>Chief Sponsor</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/embauche.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Finance Partner</h3>
        <p>Embauche Retailers pvt. Ltd</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/redwolf.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Redwolf</h3>
        <p>Event Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/buckaroo.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Buckaroo</h3>
        <p>Footwear Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/bfy.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Better Fitness For You</h3>
        <p>Fitness Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/acres.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>The Acres Club</h3>
        <p>Entertainment Partner</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/aquapharm.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Aquapharm</h3>
        <p>Chief Sponsor</p>
      </div>
    </div>
    <div class="csi-sponsor-content">
      <div class="image-container">
        <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/efz_agro.png') }}" alt=""></a>
      </div>
      <div class="csi-sponsor-content-info">
        <h3>Efzee agro</h3>
        <p>Website Partner</p>
      </div>
    </div>

  </div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection
