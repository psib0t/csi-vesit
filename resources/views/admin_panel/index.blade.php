@extends('template.template')

@section('head')
  @include('admin_panel.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
@if(Session::has('message'))
  <p class="message">{{ Session::get('message') }}</p>
  @endif

  @if(Session::has('alert'))
  <p class="alert">{{ Session::get('alert') }}</p>
  @endif

  @if (count($errors) > 0)
      <div class="alert_danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>

  @endif

  <script>
    $(document).ready(function(){
        if($("p[class='message']").html())
      {

        setTimeout( "$('.message').fadeOut();", 4000);
      }
        if($("p[class='alert']").html())
      {

        setTimeout( "$('.alert').fadeOut();", 4000);
      }



    });

    </script>


  <div class="section" >
  <h1> Admin Panel </h1>
  <p> Handle Users</p>

  <div class="council_members">
    <form method='POST' action="{{ route('make_changes') }}" id='Members_form' >
      <div style='overflow-x: scroll;'>
        <table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </table>
      </div>
    </form>
  </div>

  <p> Add Members </p>
  <form class="admin_panel_form" method="POST" action="{{ route('add_members') }}" enctype="multipart/form-data" id="add_mem">
     {{ csrf_field() }}
    <input type='text' placeholder='Name' name='mname' class='name' value="{{ old('mname') }}" ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <select name="post">
      <option value="" disabled selected>Post</option>
      <option value="Staff Incharges">Staff Incharges</option>
      <option value="B.E. Council">B.E. Council</option>
      <option value="Top4">Top4</option>
      <option value="Senior Council">Senior Council</option>
      <option value="Junior Council">Junior Council</option>

    </select><br>
    <div class='hr'></div><div class='hr'></div><br><br>
    <input type='text' placeholder='Email address' name='email' class='email' value="{{ old('email') }}" ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <input type='text' placeholder='Facebook Account' name='facebook'  class='facebook' value="{{ old('facebook') }}" ><br>
		<div class='hr'></div><div class='hr'></div><br>
		<input type='text' placeholder='Twitter Account' name='twitter'  class='twitter' value="{{ old('twitter') }}" ><br>
		<div class='hr'></div><div class='hr'></div><br>
		<input type='text' placeholder='Instagram Account' name='linkedin'  class='instagram' value="{{ old('linkedin') }}" ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <input type='text' placeholder='Username' name='username' class='username' value="{{ old('username') }}" ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <input type='password' placeholder='Password' name='password'  class='password' value="{{ old('password') }}" ><i class='fa fa-eye' aria-hidden='true' style='margin-left: -40px;color:blue;font-size: 25px;' ></i><br>
    <div class='hr'></div><div class='hr'></div><br>
    <textarea name="achievements" placeholder="Achievements"  >{{{ Input::old('achievements') }}}</textarea><br>

    <div class="remaining_characters">Remaining Characters : <b>255</b></div><br><br>

    <input type='file' name='image' style='border:0px;'  >
    <br>
    <button type='submit' class='account_button shifted' >Create Account</button>
  </form>
  <p> Handle Events </p>

  <div class="events">
    <form method='POST' action="{{ route('changing_events') }}" id='Events_form' >
      <div style='overflow-x: scroll;'>
        <table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </table>
      </div>
    </form>
  </div>
  <p> Add Events </p>





  <form class="admin_panel_form" method="POST" action="{{ route('add_events') }}" enctype="multipart/form-data" >
     {{ csrf_field() }}
    <input type='text' placeholder='Name of the event' name='ename' class='name' value="{{ old('ename') }}" ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <input type='text' placeholder='Event date and time' name='date' class='post' value="{{ old('date') }}" id='datepicker' ><br>
    <div class='hr'></div><div class='hr'></div><br>
    <textarea name="description" placeholder="Describe the event here"  >{{{ Input::old('description') }}}</textarea><br>

    <div class="remaining_characters">Remaining Characters : <b>255</b></div><br><br>
    <input type='file' name='image' style='border:0px;'>
    <br>

    <button type='submit' class='account_button shifted' >Add Event</button>
  </form>
  <p> Delete Events </p>

  <form class="admin_panel_form"   method="POST" action="{{ route('delete_events') }}" enctype="multipart/form-data" >
     {{ csrf_field() }}
    <input type='text' placeholder='Name of the event' name='ename' class='name' value="{{ old('ename') }}"><br>
    <div class='hr'></div><div class='hr'></div><br>


    <button type='submit' class='account_button shifted' >Delete Event</button>
  </form>
    <p> Handle Winners </p>

  <div class="winners">
    <form method='POST' action="{{ route('changing_winners') }}" id='Winners_form' >
      <div style='overflow-x: scroll;'>
        <table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </table>
      </div>
    </form>
  </div>
  <p> Update Winners table </p>


  <form class="admin_panel_form" id="events_form" method="POST" action="{{ route('add_winners') }}" enctype="multipart/form-data" >
     {{ csrf_field() }}
    <div id="input_fields">
    <input type='text' placeholder='Name of the event' name='wname' class='name' value="{{ old('wname') }}"><br>
    <div class='hr'></div><div class='hr'></div><br>

    </div>
    <button class='account_button shifted' id='add_fields'>Add Team</button>
    <button type='submit' class='account_button' >Update Winners</button>
  </form>


  @include('admin_panel.create_admin_table')
  @include('admin_panel.create_events_table')
  @include('admin_panel.create_winners_table')
  </div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection

@section('scripts')
  @include('admin_panel.scripts')
@endsection
