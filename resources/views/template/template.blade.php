<!DOCTYPE html>
<html>
<head>
	<title>CSI</title>
	<link rel="shortcut icon" href="{{URL::to('css/resources/images/csi-logo.png')}}">
	<script src="{{URL::to('js/jquery-2.1.1.js')}}"></script>
  @yield('head')
</head>
<body>
  @yield('nav')
  @yield('mainContent')
	@yield('footer')

	<script src="{{URL::to('js/navbar.js')}}"></script>
	<script type="text/javascript">
		var url = "{{url('/login')}}";
		var adminUrl = "{{route('admin.login.submit')}}";
	</script>
	<script src="{{URL::to('js/login.js')}}"></script>
	@yield('scripts')
</body>
</html>
