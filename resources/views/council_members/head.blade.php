<link rel="stylesheet" type="text/css" href="{{ URL::to('css/nav.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::to('css/footer.css') }}">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ URL::to('css/font-awesome/css/font-awesome.min.css') }}">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ URL::to('css/council_members.css') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
