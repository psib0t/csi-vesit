<script>
  var data = <?php print_r(json_encode($members))?>;
  var profUrl = "{{ url('/profile_page')}}/";
</script>
<script>
  function add_images(){

      <?php
      $data = $members;
      $count = 1;
      foreach($data as $member)
      {
        ?>
        var image_path = "{{ url($member->image) }}";
        var id = '<?php echo $count ?>';

        $(".photo_container."+id).css("background-image", "url("+image_path+")");
        alert('background changed')
        <?php
        $count++;
      }

      ?>
    }

  function add_hrefs(){

    <?php
    $data = $members;
    $count = 1;
    foreach($data as $member)
    {
      $id = $member->id;

      ?>

      var href = profUrl + '<?php echo $id ?>';

      var id = '<?php echo $count ?>';

      $(".photo a[id="+id+"]").attr("href",href);
      <?php
      $count++;
    }

    ?>
    add_hover_effect();
  }

  function add_hover_effect(){

    $(".photo i").hover(function(){

              $(this).prev().css("opacity",1);
              $(this).css("color","white");

            },function(){
              $(this).prev().css("opacity",0);
              $(this).css("color","#191F4D");
            });
  }

</script>
{{ Html::script('js/council_members.js') }}
<script>

  $(document).ready(function(){
    add_images();

    add_hrefs();

    add_hover_effect();
  });


</script>
