<script>
  var datas = <?php print_r(json_encode($members))?>;
  var profUrl = "{{ url('/profile_page')}}/";
  datas= datas.reduce((obj, iter)=>{
    obj[iter.category] = obj[iter.category] || [];
    obj[iter.category].push(iter);
    return obj;
  }, {});

</script>

<script>
  $(document).ready(function(){
    for(var i = 1; i <= 5; i++){
        $(".member_container").append('<div class="main '+i+'"></div>');
    }
    for (var data in datas) {
      datas[data].forEach((indata)=>{
        $(".main."+data).append("<div class='container'></div>");
        $(".main."+data+" .container:last").append("<div class='photo'></div>");
        $(".main."+data+" .container:last .photo").append("<div class='photo_container'></div>");
        $(".main."+data+" .container:last .photo .photo_container").css("background-image", "url("+indata.image+")");
        $(".main."+data+" .container:last").append("<div class='info'></div>");
        $(".main."+data+" .container:last .info").append("<p>"+ indata.mname + "</p><h3>"+ indata.post+"</h3>");
        $(".main."+data+" .container:last .info").append("<a href='"+profUrl + indata.id + "' style=' height:47px; width:47px;border-radius:23.5px; position:absolute;bottom:5px;right:5px;z-index:50;'><div style='background-color:#191F4D;opacity:0;height:40px;width:40px;border-radius:20px;z-index:209;position:absolute;bottom:5px;right:0px'></div><i class='fa fa-plus-circle profile_links' style='opacity:1;color:#191F4D;font-size:50px;position:absolute;right:0px;bottom:60px;z-index:210;' aria-hidden='true'></i></a>");

      });
    }
    var offset1 = $(".main.1").offset().top;
    var offset2 = $(".main.2").offset().top;
    var offset3 = $(".main.3").offset().top;
    var offset4 = $(".main.4").offset().top;
    var offset5 = $(".main.5").offset().top;
    var offsetBottom = $(".main.5").height() + $(".main.5").offset().top;
    console.log(offset4);
    console.log(offsetBottom);
    window.addEventListener("scroll", function(){
      //var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
      var scrollTop  = $(window).scrollTop();
      if(scrollTop > offsetBottom){
        $(".council_members_header").hide();
      }else{
        $(".council_members_header").show();
        if(scrollTop <= offset2)
        {
          $(".council_members_header").html("Staff Incharges");

        }
        else if(scrollTop <= offset3 )
        {
          $(".council_members_header").html("B.E. Council");
        }
        else if(scrollTop <= offset4 )
        {
          $(".council_members_header").html("Top4");
        }
        else if(scrollTop <= offset5 )
        {
          $(".council_members_header").html("Senior Council");
        }
        else if(scrollTop <= offsetBottom)
        {
          $(".council_members_header").html("Junior Council");
        }
      }
    });
  })
</script>
