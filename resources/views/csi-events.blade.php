<html>
    <head>
       <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <link rel="shortcut icon" href="{{URL::to('css/resources/images/csi-logo.png')}}">
    <title>Events</title>
    <!--<script src="spg/js/modernizr.min.js"></script>
    <script src="spg/js/classie.js"></script>
    <script src="spg/js/photostack.js"></script>-->
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/nav.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/login.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/footer.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/font-awesome/css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    {{ Html::script('js/modernizr.min.js') }}
    {{ Html::script('js/classie.js') }}
    {{ Html::script('js/photostack.js') }}
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/component.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
    @include('partials.nav')
    <section id="photostack-3" class="photostack fluid_container">
        <div>
            <?php $i=0; ?>
            @foreach($events as $event)
                <figure id ="{{$i}}" class="psfig">
                    <span class="helper"></span>
                    <img src="{{$event->image}}" alt="img01"/>
                </figure>
                <?php $i++; ?>
            @endforeach
        </div>
    </section>
    <div class="arrows fluid_container">
        <span class="left">
                <img src="{{URL::to('css/resources/images/la.png')}}" onclick="prevcard()">
        </span>
        <span class="right">
                <img src="{{URL::to('css/resources/images/ra.png')}}" onclick="nextcard()">
        </span>
    </div>
    <h1 id="eventname">OUR EVENTS</h1><br>
    <h2 id="event_date"></h2>
    <p id="description"></p>

    <div class="pop_up_window_container"></div>
    <div class="pop_up_window_div">
    <center>
       @include('partials.login')
    </center>
    </div>
    @include('partials.footer')
    <!-- <table class="events">
        <tr>
            <th>Event</th>
            <th>Date</th>
            <th>Description</th>
        </tr>

        @foreach($events as $event)
            <tr>
                <td>{{ $event->ename }}</td>
                <td>{{ $event->date }}</td>
                <td>{{ $event->description }}</td>
            </tr>
        @endforeach
    </table><br> -->
    <script type="text/javascript">
    var a = [];
      @foreach($events as $event)
        var event = {
          name: "{{$event->ename}}",
          description: "{{$event->description}}",
          date: "{{$event->date}}"
        };
        a.push(event);
      @endforeach
    console.log(a);
    </script>
<script>
    // [].slice.call( document.querySelectorAll( '.photostack' ) ).forEach( function( el ) { new Photostack( el ); } );
    var i = 0;
    var eventNameID = document.getElementById("eventname");
    var eventDescID = document.getElementById("description");
    var eventDateID = document.getElementById("event_date");
    eventNameID.innerHTML = a[i].name;
    eventDescID.innerHTML = a[i].description;
    eventDateID.innerHTML = a[i].date;
    new Photostack( document.getElementById( 'photostack-3' ), {
        callback : function( item ) {
            console.log(item)
        }
    } );
    function prevcard() {
        var nav = document.getElementById("dotnav").getElementsByTagName("SPAN");
        var cnav = document.getElementById("dotnav").getElementsByClassName("current");
        var icnav;
        for(var key in nav)
        {
            if(nav[key]==cnav[0])
                icnav=key;
        }
        icnav--;
        if(icnav<0)
            icnav=0;
        cnav[0].className="";
        nav[icnav].className="current";
        nav[icnav].click();
        if(i > 0){
          i--;
          eventNameID.innerHTML = a[i].name;
          eventDescID.innerHTML = a[i].description;
        }
    }
    function nextcard() {
        var nav = document.getElementById("dotnav").getElementsByTagName("SPAN");
        var cnav = document.getElementById("dotnav").getElementsByClassName("current");
        var navlength = document.getElementsByClassName("psfig").length;
        var icnav;
        for(var key in nav)
        {
            if(nav[key]==cnav[0])
                icnav=key;
        }
        icnav++;
        if(icnav>=navlength)
            icnav=navlength-1;
        cnav[0].className="";
        nav[icnav].className="current";
        nav[icnav].click();
        if(i < a.length - 1){
          i++;
          eventNameID.innerHTML = a[i].name;
          eventDescID.innerHTML = a[i].description;
        }

    }
</script>
<script src="{{URL::to('js/navbar.js')}}"></script>
<script type="text/javascript">
  var adminUrl = "{{route('admin.login.submit')}}";
</script>
<script src="{{URL::to('js/login.js')}}"></script>
</body>
</html>
