@extends('template.template')

@section('head')
  @include('home.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
  <div class="pop_up_window_container"></div>
  <div class="pop_up_window_div">
  <center>
     @include('partials.login');
  </center>
  </div>
  <div class = "csi-slider">
    <ul class="csi-slider-main">
      <li class="selected">
        <div class="csi-slider-container">
          <div class="transparent"></div>
          <div class="csi-center-div">
            <div class="csi-center-contents col-1">
              <h2>Welcome {{(Auth::check())?(Auth::user()->firstname):((Auth::guard('member')->check())?Auth::guard('member')->user()->mname:("to CSI VESIT")) }}</h2>
            </div>
            <div class="csi-center-contents col-2">
              <p>The CSI VESIT student chapter strives to embody the values of it's esteemed parent society by attempting to bring about technical and holistic awareness to it's members through a multitude of diverse events.</p>
              <p> The workshops and events conducted through the ceaselessly enthusiastic efforts of the student council bolsters an atmosphere of innovation and enterprise within the society for our members and the student community at large</p>
            </div>
          </div>
        </div>
      </li>
      <li class="csi-bg-video">
        <div class="transparent"></div>
        <div class="csi-bg-video-wrapper" data-video="{{URL::to('css/resources/videos/dark')}}">
          <!-- video element will be loaded using jQuery -->
        </div>
      </li>
      <li class="csi-upcoming-events">
        <div class="transparent"></div>
        <div class="csi-upcoming-events-container">
          <div class="event-element">
            <div class="event-image">
              <img src="{{ URL::to('/css/resources/images/sponsors/bowen-logo-2016.svg') }}" class="selected" alt="">

            </div>
          </div>
          <div class="event-content">
            <div class="event-content-text">
              <h2>Lan Gaming</h2>
              <p>24th Jan 2016</p>
            </div>
          </div>
        </div>
      </li>
      <li></li>
    </ul>
    <div class = "csi-slider-nav">
      <nav>
        <span class = "marker item-1"></span>
        <ul>
          <li class = "selected">
            <a href="#">
              <i class="fa fa-home" aria-hidden="true"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-youtube-play" aria-hidden="true"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-calendar" aria-hidden="true"></i>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="csi-about-us">
    <div class="container">
      <h1>ABOUT US</h1>
      <p>The Computer Society of India stands Numero Uno , as the first and the largest body of Computer professionals in India. </p>
    </div>
    <div class="csi-more-button">
      <a class="button" href="{{url('/about')}}">READ MORE</a>
    </div>
  </div>
  <div class="slider">
    <h1 class="blog-title">FACEBOOK FEEDS</h1>
		<ul class="blog_ul">

			<div class="blog_div">

				<li class="blog_li">


					<div  class="info" id="1">
						<div class="description">
						</div>
						<div class="author">
						</div>
					</div>
				</li>
				<li class="blog_li">


					<div  class="info" id="2">
						<div class="description">

						</div>
						<div class="author">
						</div>
					</div>
				</li>
				<li class="blog_li">


					<div  class="info" id="3">
						<div class="description">


						</div>
						<div class="author">
						</div>
					</div>
				</li>
				<li class="blog_li">


					<div  class="info" id="4">
						<div class="description">
						</div>
						<div class="author">
						</div>
					</div>
				</li>
				<li class="blog_li">


					<div  class="info" id="5">
						<div class="description">
						</div>
						<div class="author">
						</div>
					</div>
				</li>
				<li class="blog_li">


					<div  class="info" id="6">
						<div class="description">
						</div>
						<div class="author">
						</div>
					</div>
				</li>
			</div>

		</ul>
		</div>
			<div class="dots_container">
				<div class="centered_div">
					<div class="dot" id="dot1">
					</div>
					<div class="dot" id="dot2">
					</div>
					<div class="dot" id="dot3">
					</div>
					<div class="dot" id="dot4">
					</div>
					<div class="dot" id="dot5" >
					</div>
					<div class="dot" id="dot6">
					</div>
				</div>
			</div>

		<div class="data"></div>
  <div class="csi-sponsors">
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/lubricants.png') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/acres.png') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/buckaroo.png') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/redwolf.png') }}" alt=""></a>
    </div>
    <div class="csi-sponsor-holder">
      <a href="#"><img src="{{ URL::to('/css/resources/images/sponsors/aquapharm.png') }}" alt=""></a>
    </div>


    <div class="csi-all-sponsors">
      <div class="csi-all-sponsors-link">
        <a href="{{ route('home.sponsors') }}">All Sponsors <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
      </div>
    </div>
  </div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection

@section('scripts')
  @include('home.scripts')
@endsection
