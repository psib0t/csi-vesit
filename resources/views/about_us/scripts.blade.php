<script>
     function initMap() {
       var uluru = {lat: 19.0455209, lng: 72.8889294};
       var map = new google.maps.Map(document.getElementById('map-div'), {
         zoom: 17,
         center: uluru
       });
       var marker = new google.maps.Marker({
         position: uluru,
         map: map
       });
     }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1v8bcVyDcDxWmd5znaoRVwYceFIPXNlU&callback=initMap"
 type="text/javascript"></script>
