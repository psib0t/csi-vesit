@extends('template.template')

@section('head')
  @include('about_us.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
<div class="pop_up_window_container"></div>
<div class="pop_up_window_div">
<center>
   @include('partials.login');
</center>
</div>
<div class="bg-image">
</div>
<div class="about-us-div">
  <div class="about-us-title">
    <h1>About us</h1>
  </div>
  <div class="col col-1">
    <p>
      The Computer Society of India stands Numero Uno , as the first
      and the largest body of Computer professionals in India .
    </p>
    <p>
      Formed in March 1965, by a handful of Computer & IT professionals,
      today, the CSI spans over 66 chapters all over India with 381 student branches, and more than 60,000 members,
      including India’s stalwarts amongst the most famous IT industry leaders, brilliant scientists and dedicated academicians.
    </p>
    <p>
       The CSI has been instrumental in guiding the Indian IT industry down the right path during its formative years.
    </p>
    <p>
      The mission of the Computer Society India is to facilitate research, knowledge sharing,
       learning and career enhancement for all categories of IT professionals,
      while simultaneously inspiring and nurturing new entrants into the industry
      and helping them to integrate into the IT community.
    </p>
  </div>
  <div class="col col-2">
    <p>
      The Computer Society of India – VESIT Student Chapter, which is one of the most vibrant amongst
      all the student Chapters in Mumbai, is committed to the mission of creating technical awareness among students and providing them with a unique opportunity
      to learn various techniques in formulating IT strategy, helping them keep abreast of the
      latest technologies in various avenues.
    </p>
    <p>
      The activities conducted for the students associated with the Society include lecture meetings, seminars, conferences,
      training programs, workshops, paper presentation contests, programming events and practical visits to installations.
    </p>
  </div>
</div>
<div id="map-div">

</div>
@endsection

@section('footer')
  @include('partials.footer')
@endsection

@section('scripts')
  @include('about_us.scripts')
@endsection
