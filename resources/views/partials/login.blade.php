<div class="pop_up_window">
      <div style="width:300px;height:50px;line-height:50px;" class="login_header">
      <h3> Log In </h3>
      <a id="cancel"><i class="fa fa-times cross_mark" aria-hidden="true" style="color:#BDBDC8;"></i></a>
      </div>
      <form method='POST' action="{{ url('/login') }}">
        <center>
        <div class="user">

          <i class="fa fa-user" aria-hidden="true"></i>
          <input type="email" class="username" name="email" placeholder="email">

        </div>
        <div class="hr" style='height:0px;border:1px solid blue;margin:0px;display:inline;float:left;display:none;margin-left:35px;width:115px;'></div>
        <div class="hr" style='height:0px;border:1px solid blue;margin:0px;display:inline;float:left;width:115px;display:none;'></div>

        </center>
        <center>
        <div class="pass">

          <i class="fa fa-eye" aria-hidden="true"></i>
          <input type="text" class="password" name="password" placeholder="Password">

        </div>
        <div class="hr" style='height:0px;border:1px solid blue;margin:0px;display:inline;display:none;float:left;margin-left:35px;width:115px;'></div>
        <div class="hr" style='height:0px;border:1px solid blue;margin:0px;display:inline;float:left;display:none;width:115px;'></div>
        </center>
        <center>
        <div>

          <button class="login_button">Submit</button>
        </div>
        </center>
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
      </form>
</div>
