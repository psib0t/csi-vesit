  <div class = "csi-footer">
    <div class = "csi-footer-content">
      <div class="footer-social">
        <div class="fotter-social-media-icons">
          <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
        </div>
      </div>
      <div class="csi-contact-info">
        <h3>Get in touch today!</h3>
        <div class="footer-block">
          <div class="csi-vcard">
            <div class="csi-vcard-info">
              <div class="vcard-label">Pranay Mahaldar (Chairperson):</div>
              <div class="vcard-value">+91-7715930483</div>
            </div>
            <div class="csi-vcard-info">
              <div class="vcard-label">Shantanu Shende (Co-Chairperson):</div>
              <div class="vcard-value">+91-9820330567</div>
            </div>
            <div class="csi-vcard-info">
              <div class="vcard-label">Aditya Krishnan (Secretary):</div>
              <div class="vcard-value">+91-9769215694</div>
            </div>
            <div class="csi-vcard-info">
              <div class="vcard-label">Ashwini Vedula (Treasurer):</div>
              <div class="vcard-value">+91-9619270065</div>
            </div>
          </div>
        </div>
        <div class="footer-block">
          <div class="csi-vcard">
            <div class="csi-vcard-info">
              <div class="vcard-label">Address:</div>
              <div class="vcard-value">CSI Vesit | Computer Society of India-VESIT, Student Activity Centre, Collectors Colony, Chembur, Mumbai - 400074</div>
            </div>
            <div class="csi-vcard-info">
              <div class="vcard-label">Email:</div>
              <div class="vcard-value">editors.csi@gmail.com</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
