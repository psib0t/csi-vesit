<html>
<head>
	<title>
			Invalid Route
		</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<style>
			.error{
				width:90%;
				margin:100px auto;
				font-size: 15px;
				font-family: Arial, Helvetica, sans-serif;
				border:1px solid gray;
				padding:10px;
				border-radius:5px;

			}
			a{
				text-decoration: none;
			}
		</style>
</head>
<body>
<div class="error">
<h1> The page you are trying to access does not exist </h1>
<a href="{{ route('home.council_members') }}">{{ route('home.council_members') }}</a><br>
<a href="{{ route('home.eventwinners') }}">{{ route('home.eventwinners') }}</a><br>
<a href="{{ route('home.index') }}">{{ route('home.index') }}</a><br>
<a href="{{ route('home.about') }}">{{ route('home.about') }}</a><br>
<a href="{{ route('eventslider') }}">{{ route('eventslider') }}</a><br>
<a href="{{ route('home.eventwinners') }}">{{ route('home.eventwinners') }}</a><br>
</div>
</body>
</html>
