@extends('template.template')

@section('head')
  @include('profile_page.head')
@endsection

@section('nav')
  @include('partials.nav')
@endsection

@section('mainContent')
@if (count($errors) > 0)
      <div class="alert_danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>

  @endif
  @if(Session::has('message'))
  <p class="flash_message">{{ Session::get('message') }}</p>
  @endif
  <div class="details">

    <div class="user_image">
      <img src="{{ url($user['image']) }}">
    </div>

  </div>

  <div class="main_container">
    <button class="tabs" id="credentials">Credentials</button>
    <button class="tabs" id="posts" style='margin-left: -4px'>Achievements</button>
    <div class="credentials">
      <form class="credentials_form" method="POST" action="{{ route('edit_profile') }}" enctype="multipart/form-data" >
     {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $user['id'] }}">
     <table>
        <tr>
          <td>
            <label for="mname" >Name</label>
            </td>
            <td class='info'>
        <input type='text' name='mname' class='name' id="mname" disabled value="{{ $user['name'] }}">
        <div class='hr'></div><div class='hr'></div>
          </td>
      </tr>

      <tr>
        <td>
        <label for="email">Email</label>
        </td>
        <td class='info'>
        <input type='text' name='email' class='email' id="email" disabled value="{{ $user['email'] }}">
        <div class='hr'></div><div class='hr'></div>
        </td>
      </tr>

      <tr>
        <td >
        <label for="username">Username</label>
        </td>
        <td class='info'>

        <input type='text'  name='username' class='username'  id="username" disabled value="{{ $user['username'] }}">
        <div class='hr'></div><div class='hr'></div>
        </td>

      </tr>
      <tr>
        <td >
        <label for="change_password">Current Password</label>
        </td>
        <td class='info password_td'>

        <input type='password'  name='current_password' class='password change_password'  id="change_password" disabled>
        <i class="fa fa-eye" aria-hidden="true"></i>
        <div class='hr'></div><div class='hr'></div>
        </td>

      </tr>
      <tr>
        <td >
        <label for="confirm_password">New Password</label>
        </td>
        <td class='info password_td'>

        <input type='password'  name='new_password' class='password confirm_password'  id="confirm_password" disabled>
        <i class="fa fa-eye" aria-hidden="true"></i>
        <div class='hr'></div><div class='hr'></div>
        </td>

      </tr>

      <tr>
        <td>
        </td>
        <td class='info'>

        <button type='submit' class='save_changes' disabled >Save Changes</button>
        </td>
      </tr>
      </table>
  </form>



    <button class="login_form_trigger_edit">Edit</button>
    </div>
    <div class="posts">
      <form class="achievements_form" method="POST" action="#" enctype="multipart/form-data" >
        {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $user['id'] }}">
        <textarea name='achievements' disabled > {{ $user['achievements'] }}</textarea>


        <button type='submit' class="edit_achievements save_changes" disabled>Save Changes</button>
          </form>
          <button class="login_form_trigger">Edit</button>
    </div>
  </div>

  <!-- code for the pop up -->
  <div class="pop_up_window_container">



	</div>
	<div class="pop_up_window_div">
	<center>
	   @include('partials.login');
	</center>
	</div>
    <?php  if($user['facebook'])
			{

				?>
				<script>
				var facebook = '<?php echo $user['facebook'] ?>';
				var img = "{{url('css/resources/images/facebook.png')}}";
				$(".details").append("<a href="+facebook+" class='social_media_links'><img class='social_media_links_imgs' src="+img+"></a>");
				</script>
				<?php
			}
			if($user['twitter'])
			{

				?>
				<script>
				var twitter = '<?php echo $user['twitter'] ?>';
				var img = "{{url('/images/twitter.png')}}";
				$(".details").append("<a href="+twitter+" class='social_media_links'><img class='social_media_links_imgs' src="+img+"></a>");
				</script>
				<?php
			}
			if($user['linkedin'])
			{
				?>
				<script>
				var linkedin = '<?php echo $user['linkedin'] ?>';
				var img = "{{url('/images/linkedin.png')}}";
				$(".details").append("<a href="+linkedin+" class='social_media_links'><img class='social_media_links_imgs' src="+img+"></a>");
				</script>
				<?php
			}
		?>
@endsection

@section('footer')
  @include('partials.footer')
@endsection

@section('scripts')
  @include('profile_page.scripts')
@endsection
