<?php
  if(Auth::guard('member')->check() && Auth::guard('member')->user()->id == $user['id']){
    $user['editable'] = 'YES';
  }
?>

<?php

  if($user['editable'] == 'YES')
  {
    ?>
      <script>
      $(".login_form_trigger").prop("disabled",true);
      $("input").prop("disabled",false);
      $("textarea").prop("disabled",false);
      $("select").prop("disabled",false);
      $(".save_changes").prop("disabled",false);
      </script>
    <?php
  }
?>
{{ Html::script('js/profile_page.js') }}

<script>
// setting the post field
var post = '{{ $user["post"] }}';
$(document).ready(function (){
  $("select option[value='"+post+"']").attr("selected","selected");


});
