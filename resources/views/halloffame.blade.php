<html>
    <head>
       <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <link rel="shortcut icon" href="{{URL::to('css/resources/images/csi-logo.png')}}">
    <title>Events</title>
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/font-awesome/css/font-awesome.min.css') }}">
    <link href="css/hofcss.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/nav.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/footer.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::to('css/login.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <script src="{{URL::to('js/jquery-2.1.1.js')}}"></script>
    </head>
    <body>
    @include('partials.nav')
    <div class="pop_up_window_container"></div>
    <div class="pop_up_window_div">
    <center>
       @include('partials.login');
    </center>

    </div>
    <div class="bg-image"></div>
    <div class="winner-container">
        <h1>Hall Of Fame</h1>

        @foreach($winners as $wr)

                <div class=" row winner-row">

                <h2>{{$wr->wname}}</h2><br>
                <div class="winner-flex-container">
                <div class="winner-div " >
                    <div class="image-container gold">
                        <i class="fa fa-trophy "  aria-hidden="true"></i>
                    </div>
                    <h3>{{ $wr->team1}}</h3>
                </div>
            @if($wr->team2 != 'NULL')
                 <div class="winner-div ">
                    <div class="image-container silver">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                    </div>
                    <h3>{{ $wr->team2 }}</h3>
                </div>
            @endif

           @if($wr->team3 != 'NULL')
                <div class="winner-div ">
                    <div class="image-container bronze">
                        <i class="fa fa-trophy" aria-hidden="true"></i>
                    </div>
                    <h3>{{ $wr->team3 }}</h3>
                </div>
           @endif
            </div>
                </div>

                <br><br>

        @endforeach
        </div>
    </div>

    <script src="{{URL::to('js/navbar.js')}}"></script>
    <script type="text/javascript">
      var adminUrl = "{{route('admin.login.submit')}}";
    </script>
    <script src="{{URL::to('js/login.js')}}"></script>
</body>
</html>
