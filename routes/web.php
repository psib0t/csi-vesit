<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
  'uses' => 'MainController@getHome',
  'as' => 'home.index'
]);

Route::get('/about', function(){
  return view('about_us.index');
})->name('home.about');


Route::get('/sponsors', [
  'uses' => 'MainController@getSponsors',
  'as' => 'home.sponsors'
]);

Route::get('/events','MainController@getpics')->name('eventslider');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/admin/login', [
    'uses' => 'Auth\MemberLoginController@showLoginForm',
    'as' => 'admin.login'
]);
Route::post('/admin/login', [
    'uses' => 'Auth\MemberLoginController@login',
    'as' => 'admin.login.submit'
]);

Route::post('/admin/Event_added_to_the_database', 'AdminController@addEvents')->name('add_events');

Route::post('/admin/Event_deleted_from_the_database', 'AdminController@deleteEvents')->name('delete_events');
Route::post('/admin/Winners_table_was_updated', 'AdminController@addWinners')->name('add_winners');

Route::post('/admin/Events_table_was_altered', 'AdminController@changeEvents')->name('changing_events');
Route::post('/admin/Winners_table_was_altered', 'AdminController@changeWinners')->name('changing_winners');
Route::post('/admin/member_added_to_the_database','AdminController@addMembers')->name('add_members');

Route::post('/admin/making_changes_to_the_database','AdminController@makeChanges')->name('make_changes');
Route::get('/admin', [
    'uses' => 'AdminController@showAdminPanel',
    'as' => 'admin.dashboard'
]);

Route::get('/halloffame','MainController@getwinners')->name('home.eventwinners');
Route::get('/council_members', 'MainController@getCouncilMembers')->name('home.council_members');
Route::get('/profile_page/{id}', 'MainController@showProfile')->name('profile_page');
Route::post('/profile_page/edit_credentials', 'AdminController@editProfile')->name('edit_profile');
Route::post('/profile_page/edit_achievements', 'AdminController@editAchievements')->name('edit_achievements');
