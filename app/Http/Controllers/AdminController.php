<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\EventsModel;
use App\Winners;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    private function getCategory($name){
      $val = 5;
      switch ($name) {
        case 'Staff Incharges':
          $val = 1;
          break;
        case 'B.E. Council':
          $val = 2;
          break;
        case 'Top4':
          $val = 3;
          break;
        case 'Senior Council':
          $val = 4;
          break;

        default:
          $val = 5;
          break;
      }
      return $val;
    }

    public function showAdminPanel(){
      $members = Member::all();
      $events = EventsModel::all();
      $winners = Winners::all();
      if(Auth::guard('member')->user()->isadmin == 1)
        return view('admin_panel.index')->with(['members'=>$members,'events'=>$events,'winners'=>$winners]);
      else
        return redirect('/');
    }

    public function addMembers(Request $data)
    {
  $count = count(Member::all());

    $member = new Member;

$this->validate($data, [
          'mname' => 'bail|required|max:255',
          'post' => 'bail|required|max:255',
          'email' => 'bail|email|unique:members|required|max:255',
          'facebook' => 'bail|url|max:255',
          'twitter' => 'bail|url|max:255',
          'instagram' => 'bail|url|max:255',
          'username' => 'bail|required|unique:members|max:255',
          'achievements' => 'bail|max:255',
          'password' => 'bail|required|min:6'

      ]);

  $mname = $data['mname'];
  $post = $data['post'];
  $email = $data['email'];
  $facebook = $data['facebook'];
  $twitter = $data['twitter'];
  $linkedin = $data['linkedin'];
  $username = $data['username'];
  $password = bcrypt($data['password']);
  $achievements = $data['achievements'];

  if(Input::hasFile('image'))
  {
      $file = $data->file('image');
    $filename = 'profilePictures/' . $file->getClientOriginalName();
     $file->move('profilePictures',$filename);
  }
  else
  {
     $filename = 'css/resources/images/default.jpg';
  }
  $member->category = $this->getCategory($post);


  $member->id = $count + 1;
  $member->mname = $mname;
  $member->post = $post;
  $member->email = $email;
  $member->facebook = $facebook;
  $member->twitter = $twitter;
  $member->linkedin = $linkedin;
  $member->username = $username;
  $member->password = $password;
  $member->image = $filename;
  $member->achievements = $achievements;
  $member->save();

  $members = Member::all();

   Session::flash('message', 'A new member was added to the database');
  return redirect()->back();

}



public function makeChanges(Request $data)
{


$input = $data->all();

$num = 1;
$pass = 1;
$total_inputs = count($input);
$count = ($total_inputs-1)/3;

while($num <= $count)
{
  if(isset($input['checkbox']))
  {
    if( in_array( $num ,$input['checkbox'] ) )
    {
      $memberDelete = Member::find($num);
      $imagePath = $memberDelete->image;
      if($imagePath != 'profilePictures/default.jpg')
      {
        File::delete($imagePath);
      }
      Member::destroy($num);

      $num++;
      continue;
    }
    else
    {
      $member = Member::find($num);
       $member->id = $pass;
      $member->mname = $input['mname'.$num];

    $member->post = $input['post'.$num];

  $isadmin = $input['isadmin'.$num];
  if($isadmin == 'YES' || $isadmin == 'Y' || $isadmin == 'y' || $isadmin == 'yes' || $isadmin == 'Yes')
     {
      $isadmin = 1;
     }
  else
    {

      $isadmin = 0;
    }
  $member->isadmin = $isadmin;


  $num++;
  $pass++;
  $member->save();

    }
  }
  else{

      $member = Member::find($num);
       $member->id = $pass;
  $member->mname = $input['mname'.$num];

  $member->post = $input['post'.$num];

  $isadmin = $input['isadmin'.$num];
   if($isadmin == 'YES' || $isadmin == 'Y' || $isadmin == 'y' || $isadmin == 'yes' || $isadmin == 'Yes')
     {
      $isadmin = 1;
     }
  else
    {

      $isadmin = 0;
    }
  $member->isadmin = $isadmin;


  $num++;
  $pass++;
  $member->save();
  }
}

   Session::flash('message', 'Changes were made successfully');
return redirect()->back();


}

public function addEvents(Request $data){

    $this->validate($data, [
          'ename' => 'required|unique:events_models|max:255',
          'date' => 'required|unique:events_models|max:255',
          'description' => 'required|max:255',
          'image' => 'required'
      ]);

     $count = count(EventsModel::all());
    $event = new EventsModel;
   $event->id = $count + 1;
   $event->ename = $data['ename'];
   $event->date = $data['date'];
   $event->description = $data['description'];

   $file = $data->file('image');
    $filename = 'EventPictures/' . $file->getClientOriginalName();
     $file->move('EventPictures',$filename);

     $event->image = $filename;
      $event->save();
      Session::flash('message', 'A new event was added to the database');
      return redirect()->back();

}
public function deleteEvents(Request $data){
    $this->validate($data, [
          'ename' => 'required|max:255',

      ]);
    $eventPassed = $data['ename'];
   $event = EventsModel::where('ename',$eventPassed)->value("id");


   $pass = $event + 1;
   $count = count(EventsModel::all());

   while($pass <= $count)
   {
      $event = EventsModel::find($pass);
       $event->id = $pass - 1;
       $event->save();
       $pass++;
   }
   File::delete($eventimage);
  EventsModel::where('ename',$eventPassed)->delete();
  Session::flash('message', 'An event was deleted from the database');
  return redirect()->back();
}

public function addWinners(Request $data){
    $this->validate($data, [
          'wname' => 'required|max:255',
          'team1' => 'required|max:255',

      ]);



 $winner = new Winners;
 $count = count(Winners::all());
 $winner->id = $count + 1;
  $winner->wname = $data['wname'];
  $winner->team1 = $data['team1'];

  if(isset($data['team2']))
  {
    $winner->team2 = $data['team2'];
  }
  if(isset($data['team3']))
  {
    $winner->team3 = $data['team3'];
  }

  $winner->save();
  Session::flash('message', 'Winners table was updated');
  return redirect()->back();

}

public function changeEvents(Request $data){

 $input = $data->all();

$num = 1;

$total_inputs = count($input);
$count = ($total_inputs-1)/2;

while($num <= $count)
{


      $event = EventsModel::find($num);
       $event->id = $num;
  $event->ename = $input['ename'.$num];

  $event->date = $input['date'.$num];





  $num++;

  $event->save();

}
Session::flash('message', 'Changes were made successfully');
 return redirect()->back();
}







public function changeWinners(Request $data){


$input = $data->all();
$num = 1;

$total_inputs = count($input);
$count = ($total_inputs-1)/4;

while($num <= $count)
{


      $winner = Winners::find($num);
       $winner->id = $num;
  $winner->wname = $input['wname'.$num];

  $winner->team1 = $input['team1'.$num];
  $winner->team2 = $input['team2'.$num];
  $winner->team3 = $input['team3'.$num];





  $num++;

  $winner->save();

}
Session::flash('message', 'Winners table was updated');
 return redirect()->back();

}
public function editProfile(Request $request){
  $id = $request['id'];
         $route = '/profile_page/'.$id;

         if(empty($request['mname'])  && empty($request['post']) && empty($request['email']) && empty($request['username']) && empty($request['current_password']))
         {
            Session::flash('message', 'First five fields are not optional');


                return redirect($route);
         }
         else
         {

                $member = Member::find($id);
                if($request['username'] != $member->username)
                {
                    $othermember = Member::where('username',$request['username'])->value('username');
                    if($othermember == $request['username'])
                    {
                         Session::flash('message', 'Someone already has that username');

                            $route = '/profile_page/'.$id;
                            return redirect($route);
                    }
                    else
                    {
                        $member->mname = $request['mname'];
                        $member->email = $request['email'];
                        $member->username = $request['username'];

                        if(!empty($request['new_password']))
                        {
                            if($member->password == $request['current_password'])
                            {
                                if(count($request['new_password']) < 6)
                                {
                                    Session::flash('message', 'Password cannot have less than 6 letters');


                                    return redirect($route);
                                }
                                else
                                {
                                    $member->passowrd = $request['new_password'];
                                }
                            }
                            else
                            {
                                Session::flash('message', 'Current password is incorrect');


                                    return redirect($route);

                            }

                        }

                        $member->save();
                        Session::flash('message', 'Changes were made successfully');
                       return redirect($route);
                    }
                }
                else
                {
                        $member->mname = $request['mname'];
                        $member->email = $request['email'];
                        $member->username = $request['username'];

                            if(!empty($request['new_password']))
                            {
                            if($member->password == $request['current_password'])
                            {
                                if(strlen($request['new_password']) < 6)
                                {

                                    Session::flash('message', 'Password cannot have less than 6 letters');


                                    return redirect($route);
                                }
                                else
                                {
                                    $member->password = $request['new_password'];
                                }
                            }
                            else
                            {
                                Session::flash('message', 'Current password is incorrect');
                                    return redirect($route);
                            }
                         }
                        $member->save();
                        Session::flash('message', 'Changes were successfully recorded');
                        return redirect($route);
                }
         }

}

public function editAchievements(Request $request){
  return view('admin');
}


    public function index()
    {
        return view('admin');
    }
}
