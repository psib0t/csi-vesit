<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Winners;
use App\EventsModel;
use App\Http\Requests;
use App\Member;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function getHome(){
      $events = EventsModel::orderBy('date', 'ASC')->get();
      return view("home.index")->with('events', $events);
    }
    public function getSponsors(){
      return view("sponsors.sponsors");
    }
    public function getwinners(){
      $winners = Winners::orderBy('wname', 'ASC')->get();
      return view('halloffame',['winners' => $winners]);
    }
    public function getCouncilMembers(){
      $members = Member::all();
      return view('council_members.index')->with('members' , $members);
    }
    public function getpics()
    {
        $events = EventsModel::orderBy('date', 'ASC')->get();
        return view('csi-events',['events' => $events]);
    }
    public function showProfile($id){
      $username = DB::table('members')->where('id', $id)->value('username');
          $image = DB::table('members')->where('id', $id)->value('image');
          $post = DB::table('members')->where('id', $id)->value('post');
          $email = DB::table('members')->where('id', $id)->value('email');
          $facebook = DB::table('members')->where('id', $id)->value('facebook');
          $twitter = DB::table('members')->where('id', $id)->value('twitter');
          $linkedin = DB::table('members')->where('id', $id)->value('linkedin');
          $name = DB::table('members')->where('id', $id)->value('mname');
           $achievements = DB::table('members')->where('id', $id)->value('achievements');

              $user=array("id"=>$id,"name"=>$name,"achievements"=>$achievements,"post"=>$post,"facebook"=>$facebook,"twitter"=>$twitter,"linkedin"=>$linkedin,"email"=>$email,"image"=>$image,"username"=>$username,"editable"=>'NO');
      return view('profile_page.index')->with('user',$user);
    }
}
