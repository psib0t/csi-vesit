<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Winners;
use App\Http\Requests;

class WinnerController extends Controller
{
    public function getwinners()
    {
        $winners = Winners::orderBy('wname', 'ASC')->get();
        return view('halloffame',['winners' => $winners]);
    }
}
