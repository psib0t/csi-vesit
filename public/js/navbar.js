$(document).ready(function(){
	$(window).scroll(function(){
		if($(window).scrollTop() > 0){
		  $('.csi-header').addClass('header-color-change');
		}else{
		  $('.csi-header').removeClass('header-color-change');
		}
	});

	$('.csi-nav-trigger').click(function(){
		$('.csi-nav-trigger').toggleClass('trigger-open');
		$('.csi-header').toggleClass('csi-header-hide');
		$('.csi-nav').toggleClass('is-visible');
		$('.csi-logo').toggleClass('logo-color-change');
		$('body').toggleClass('body-overflowY-hide');
		if(isTop()){
			$('.csi-header').toggleClass('header-color-change');
		}
	});

	function isTop(){
		if($(window).scrollTop() > 0){
			return true;
		}
		return false;
	}
});