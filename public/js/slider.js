$(document).ready(function(){
	var sliderNav = $('.csi-slider-nav'),
		navMarker = $('.marker'),
		primarySlider = $('.csi-slider-main');

		uploadVideo(primarySlider);

		sliderNav.on('click', 'li', function(event){
			event.preventDefault();
			var selectedItem = $(this),
				oldItem = sliderNav.find('li.selected');
			if(!selectedItem.hasClass('selected')){
				updateSliderNavigation(selectedItem, oldItem);
				if(selectedItem.index() > oldItem.index()){
					updateNext(primarySlider.find('li.selected'), primarySlider, selectedItem.index());
				}else{
					updatePrev(primarySlider.find('li.selected'), primarySlider, selectedItem.index());
				}
				updateNavMarker(navMarker, selectedItem.index() + 1, oldItem.index() + 1);
			}
		});
		function updateSliderNavigation(selectedItem, oldItem, updateIndex){
			oldItem.removeClass('selected');
			selectedItem.addClass('selected');
		}

		function updateNavMarker(navMarker, index, oldItemIndex){
			navMarker.removeClass('item-'+oldItemIndex).addClass('item-'+index);
		}
		function updateNext(visibleSlide, primarySlider, n){
			visibleSlide.removeClass('selected from-left from-right').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				visibleSlide.removeClass('is-moving');
			});
			primarySlider.children('li').eq(n).addClass('selected from-right').prevAll().addClass('move-left');
			console.log(visibleSlide.index());
			checkVideo(visibleSlide, primarySlider, n);
		}
		function updatePrev(visibleSlide, primarySlider, n){
			visibleSlide.removeClass('selected from-left from-right').addClass('is-moving').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				visibleSlide.removeClass('is-moving');
			});
			primarySlider.children('li').eq(n).removeClass('move-left').addClass('selected from-left').nextAll().removeClass('move-left');
			console.log(visibleSlide.index());
			checkVideo(visibleSlide, primarySlider, n);
		}

		function uploadVideo(container) {
			container.find('.csi-bg-video-wrapper').each(function(){
				var videoWrapper = $(this);
				if( videoWrapper.is(':visible') ) {
					// if visible - we are not on a mobile device
					var	videoUrl = videoWrapper.data('video'),
						video = $('<video loop><source src="'+videoUrl+'.mp4" type="video/mp4" /><source src="'+videoUrl+'.webm" type="video/webm" /></video>');
					video.appendTo(videoWrapper);
					// play video if first slide
					if(videoWrapper.parent('.cd-bg-video.selected').length > 0) video.get(0).play();
				}
		});

	}
		function checkVideo(hiddenSlide, container, n) {
		//check if a video outside the viewport is playing - if yes, pause it
			var hiddenVideo = hiddenSlide.find('video');
			if( hiddenVideo.length > 0 ) hiddenVideo.get(0).pause();

			//check if the select slide contains a video element - if yes, play the video
			var visibleVideo = container.children('li').eq(n).find('video');
			if( visibleVideo.length > 0 ) visibleVideo.get(0).play();
	}
});
